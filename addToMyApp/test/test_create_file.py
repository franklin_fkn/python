import os.path

from addToMyApp.application.create_file_application import create_file

home = os.getenv('HOME')


def test_add_a_file_to_applications():
    file = create_file('Robo3t', directory_file_program = home + '/installs/robo_3t/robo3t-1.4.1-linux-x86_64-122dbd9/bin/robo3t')
    assert file is not None
