import os.path

HOME = os.getenv('HOME')
DIRECTORY_APPLICATIONS = HOME + '/.local/share/applications/'


def create_file(program_name: str, directory_file_program: str, program_extension='.desktop'):
    """
    :param program_name: name of the program to add to the applications
    :param directory_file_program: from the personal directory, is the path where executable program is located
    :param program_extension: extension choosen to save the program
    :return: the created or edited file
    """

    directory_file_program = directory_file_program

    icon_path = HOME + '/installs/robo_3t/robo3t-1.4.1-linux-x86_64-122dbd9/robo3t.png'
    program_name = program_name
    if not os.path.isdir(DIRECTORY_APPLICATIONS):
        os.mkdir(DIRECTORY_APPLICATIONS)
    complete_path = os.path.join(DIRECTORY_APPLICATIONS + program_name + program_extension)

    file = open(complete_path, 'w')

    file.write('[Desktop Entry]\n'
               'Name='+program_name+'\n'
               'Comment=Free comment about the program.\n'
               'Exec='+directory_file_program+'\n'
               'Terminal=false\n'
               'Type=Application\n'
               'Icon='+icon_path+'\n'
               'Categories=GNOME;GTK;Network;Utility\n'
               'StartupWMClass=' + program_name)
    file.close()
    return file
